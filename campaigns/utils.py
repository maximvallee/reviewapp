from django.urls import resolve
from django.template import Template, Context
from django.contrib.sites.models import Site
from django.urls import reverse
from django.core.mail import get_connection, EmailMultiAlternatives
import re
from collections import Counter
import numpy as np
from colormap import rgb2hex, rgb2hls, hls2rgb


def send_mass_html_mail(datatuple, fail_silently=False, user=None, password=None,
                        connection=None, bcc=[]):
    """
    Given a datatuple of (subject, text_content, html_content, from_email,
    recipient_list), sends each message to each recipient list. Returns the
    number of emails sent.

    If from_email is None, the DEFAULT_FROM_EMAIL setting is used.
    If auth_user and auth_password are set, they're used to log in.
    If auth_user is None, the EMAIL_HOST_USER setting is used.
    If auth_password is None, the EMAIL_HOST_PASSWORD setting is used.

    """
    # import pdb; pdb.set_trace()

    connection = connection or get_connection(
        username=user, password=password, fail_silently=fail_silently)
    messages = []

    for subject, text, html, from_email, recipient in datatuple:
        message = EmailMultiAlternatives(
            subject, text, from_email, recipient, bcc)
        message.attach_alternative(html, 'text/html')
        messages.append(message)
    return connection.send_messages(messages)


def to_snake_case(string):
    # import pdb; pdb.set_trace()
    return string.lower().replace(" ", "_")


def test_scores(evaluation, input_field, operator, passing_score):

    scores = evaluation.scores_from_evaluation.all()
    success_count = 0
    max_success_count = len(scores)
    # import pdb; pdb.set_trace()
    for score in scores:
        if operator == 0:  # Equal to
            if int(score.value) == int(passing_score):
                success_count += 1
        if operator == 1:  # Not equal to
            if int(score.value) != int(passing_score):
                success_count += 1
        if operator == 2:  # Greater than or equal to
            if int(score.value) >= int(passing_score):
                success_count += 1
        if operator == 3:  # Less than or equal to
            if int(score.value) <= int(passing_score):
                success_count += 1

    # import pdb; pdb.set_trace()
    if input_field == 1:  # All evaluations must pass
        if success_count == max_success_count:
            return True
    if input_field == 2:  # At least one evaluation must pass
        if success_count > 0:
            return True
    if input_field == 3:  # No evaluation must pass
        if success_count == 0:
            return True
    return False


def is_email_valid(email):
    regex = "(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)"
    isEmail = True if re.search(regex, email) else False
    return isEmail


def find_duplicates(arr):
    d = Counter(arr)
    dupes = [k for k, v in d.items() if v > 1]
    return dupes


def hex_to_rgb(hex):
    hex = hex.lstrip('#')
    hlen = len(hex)
    return tuple(int(hex[i:i+hlen//3], 16) for i in range(0, hlen, hlen//3))


def adjust_color_lightness(r, g, b, factor):
    # import pdb; pdb.set_trace()
    h, l, s = rgb2hls(r / 255.0, g / 255.0, b / 255.0)
    l = max(min(l * factor, 1.0), 0.0)
    r, g, b = hls2rgb(h, l, s)
    return rgb2hex(int(r * 255), int(g * 255), int(b * 255))


def darken_color(r, g, b, factor=0.1):
    return adjust_color_lightness(r, g, b, 1 - factor)


def is_format_valid(path_csv):
    # import pdb; pdb.set_trace()
    with open(path_csv) as file:
        next(file)
        reader = csv.reader(file)
        data = list(reader)

    errors_emails = []
    columns = list(zip(*data))

    try:
        # Empty File #
        if len(columns) == 0:
            raise EmptyFileError
        for iteration, col in enumerate(columns):
            # Empty Fields #
            if '' in col:
                raise EmptyFieldError()
            # Duplicates & Invalid Emails #
            if iteration == 0:
                if len(find_duplicates(col)) > 0:
                    dupes = find_duplicates(col)
                    raise EmailDuplicateError()
                for email in col:
                    if not is_email_valid(email):
                        errors_emails.append(email)
                if len(errors_emails) > 0:
                    raise EmailFormatError()
            # Invalid Template No. #
            if iteration == 1:
                for template_no in col:
                    ceil = len(templates.TEMPLATES)
                    if ceil == 0:
                        raise NoTemplatesFoundError()
                    elif not (is_in_range(template_no, ceil)):
                        raise InvalidTemplateError()

    except EmptyFileError:
        click.echo('Oops! File "{}" empty.'.format(path_csv))
        return False
    except EmptyFieldError:
        click.echo('Oops! File "{}" has empty cell(s) in column {}.'.format(
            path_csv, iteration+1))
        return False
    except EmailFormatError:
        click.echo('Oops! File "{}" has invalid email(s): {}'.format(
            path_csv, errors_emails))
        return False
    except EmailDuplicateError as Argument:
        click.echo(
            'Oops! File "{}" has duplicate email(s): {}'.format(path_csv, dupes))
        return False
    except NoTemplatesFoundError:
        click.echo('Oops! No templates were found.')
        return False
    except InvalidTemplateError:
        click.echo('Oops! Invalid template no.: "{}". Choose a template no. between 1 and {}.'.format(
            template_no, ceil))
        return False
    except Exception as e:
        click.echo('Error: {}'.format(e))
        return False

    return True


def inject_dynamic_content_to_body(email_template, recipient, review_request=None):

    empty_body = Template(email_template.body)

    context_body = Context({
        "name": recipient.first_name,
    })

    if review_request:
        extra_field_1 = recipient.extra_field_1
        extra_field_2 = recipient.extra_field_2
        extra_field1_label = review_request.campaign.extra_field1_label
        extra_field2_label = review_request.campaign.extra_field2_label
        for field in [(extra_field1_label, extra_field_1), (extra_field2_label, extra_field_2)]:
            if field[0]:
                context_body[to_snake_case(field[0])] = field[1]

    return empty_body.render(context_body)


def create_context_email(request, email_template, recipient, sender, campaign, review_request=None):
    # request: request object
    # email_template: must be an email_template object
    # recipient: can be recipient OR user object
    # sender: must be user object
    # request: review object (OPTIONAL, when test email)
    # import pdb
    # pdb.set_trace()

    context_email = {
        'subject_line': email_template.subject_line,
        'heading': email_template.heading,
        'sub_heading': email_template.sub_heading,
        'sub_body': email_template.sub_body,
        'recipient_name': recipient.first_name,
        'button_label': email_template.button,
        'heading_color': email_template.heading_color,
        'body_color': email_template.body_color,
        'background_color': email_template.background_color,
        'button_color': email_template.button_color,
        'sender_email': '{} at {} <{}>'.format(sender.first_name, sender.organization, sender.email),
        'sender_organization': sender.organization,
        'sender_name': sender.first_name,
        'sender_last': sender.last_name,
        'sender_address': sender.address,
        'sender_city': sender.city,
        'sender_state': sender.state,
        'sender_zip': sender.zip_code,
        'sender_website': sender.website,
        'sender_logo': sender.logo.url,
        'sender_avatar': sender.avatar.url,
        'display_evaluation': False if campaign.criteria_templates.count() > 1 else True,
        'criteria_label': campaign.criteria_templates.first().label,
        'routes': campaign.route.all(),
        'domain': Site.objects.get_current().domain,

        'evaluation_link': sender.website,
        'email_score1_link': sender.website,
        'email_score2_link': sender.website,
        'email_score3_link': sender.website,
        'body': inject_dynamic_content_to_body(email_template, recipient),
        'unsub_url': sender.website
    }

    if review_request:

        context_email['slug'] = review_request.slug
        url_routes = []
        if context_email['routes']:
            for route in context_email['routes']:
                url_routes.append(request.build_absolute_uri(reverse(
                    'campaigns:recapture_email', kwargs={'slug': review_request.slug, 'rpk': route.id})))

        context_email['routes'] = zip(context_email['routes'], url_routes)

        context_email['evaluation_link'] = request.build_absolute_uri(
            reverse('campaigns:evaluation_submit', kwargs={'slug': review_request.slug}))  # Recreate the URL, given the view name, ####

        context_email['comments_link'] = request.build_absolute_uri(
            reverse('campaigns:comments_submit', kwargs={'slug': review_request.slug}))

        context_email['email_score1_link'] = request.build_absolute_uri(
            reverse('campaigns:email_router', kwargs={'slug': review_request.slug, 'rating': 1}))

        context_email['email_score2_link'] = request.build_absolute_uri(
            reverse('campaigns:email_router', kwargs={'slug': review_request.slug, 'rating': 2}))

        context_email['email_score3_link'] = request.build_absolute_uri(
            reverse('campaigns:email_router', kwargs={'slug': review_request.slug, 'rating': 3}))

        context_email['unsub_url'] = request.build_absolute_uri(reverse('campaigns:ubsubscribe', kwargs={
            'pk': review_request.recipient.id, 'unsub_id': review_request.recipient.unsub_id}))

        context_email['body'] = inject_dynamic_content_to_body(
            email_template, recipient, review_request)

    # import pdb
    # pdb.set_trace()

    return context_email


def url_to_view(url):
    view_name = resolve(url).url_name
    return view_name

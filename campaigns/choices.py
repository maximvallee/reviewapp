INPUT_TYPES = [
    (0, 'Scale'),
    (1, 'Dropdown'),
    (2, 'Input'),
]

SCALE_OPTIONS = [
    (4, '3'),
    (5, '4'),
]

STATUS_OPTIONS = [
    (0, 'Unsent'),
    (1, 'Sent'),
    (2, 'Read'),
    (4, 'Clicked-through'),
    (5, 'Completed'),
]

INPUT_FIELDS = [
    (0, 'recipient email'),
    (1, 'all evaluation scores'),
    (2, 'some evaluation scores'),
    (3, 'no evaluation score'),
]

COMPARISON_OPERATORS = [
    (0, 'equal to'),
    (1, 'not equal to'),
    (2, 'greater than or equal to'),
    (3, 'less than or equal to'),
    (4, 'contains'),
]

LOGICAL_OPERATORS = [
    (0, 'AND'),
    (1, 'OR'),
]

RATING_OPTIONS = [
    (0, 'Pass'),
    (1, 'Fail'),
]

USER_STATUS = [
    (0, 'Subscribed'),
    (1, 'Unsubscribed')
]

PRIORITY_LEVEL = [
    (1, '1'),
    (2, '2'),
    (3, '3'),
    (4, '4'),
    (5, '5')
]

TIMEOUT_OPTIONS = [
    (2000, '2 sec'),
    (4000, '4 sec'),
    (6000, '6 sec'),
    (8000, '8 sec'),
    (10000, '10 sec')
]


HTML_EMAIL_OPTIONS = [
    (0, 'Template 1'),
    (1, 'Template 2'),
    (2, 'Template 3'),
]

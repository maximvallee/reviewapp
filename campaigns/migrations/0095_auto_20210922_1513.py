# Generated by Django 3.2 on 2021-09-22 19:13

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('campaigns', '0094_rename_html_templates_emailtemplate_html_template'),
    ]

    operations = [
        migrations.AddField(
            model_name='emailtemplate',
            name='template_name',
            field=models.CharField(default='New template', max_length=48),
        ),
        migrations.AlterField(
            model_name='emailtemplate',
            name='body',
            field=models.TextField(default='Hope you’re doing well. I just wanted to check in on you regarding your stay at {{address}}. How has your experience been so far? I know you must be really busy but I would really appreciate it if you could take just 2 mins to fill out this short survey.', max_length=750),
        ),
        migrations.AlterField(
            model_name='emailtemplate',
            name='html_template',
            field=models.SmallIntegerField(choices=[(0, 'Template 1'), (1, 'Template 2')], default=0),
        ),
        migrations.AlterField(
            model_name='emailtemplate',
            name='sub_body',
            field=models.TextField(default='Whether it is to express positive comments or provide constructive criticism, your feedback allows us to improve the quality of our rental community. ', max_length=750),
        ),
        migrations.AlterField(
            model_name='emailtemplate',
            name='subject_line',
            field=models.CharField(default='Message from Isabella', max_length=48),
        ),
    ]

# Generated by Django 3.2 on 2021-04-10 16:40

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('campaigns', '0018_remove_reviewrequest_routedto'),
    ]

    operations = [
        migrations.AddField(
            model_name='reviewrequest',
            name='routedTo',
            field=models.CharField(blank=True, max_length=48, null=True),
        ),
    ]

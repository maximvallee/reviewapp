# Generated by Django 3.2 on 2021-06-01 22:13

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('campaigns', '0084_auto_20210601_1805'),
    ]

    operations = [
        migrations.AlterField(
            model_name='introtemplate',
            name='subheading_color',
            field=models.CharField(default='#89898B', max_length=7),
        ),
    ]

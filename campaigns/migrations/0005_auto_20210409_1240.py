# Generated by Django 3.2 on 2021-04-09 16:40

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('campaigns', '0004_auto_20210408_2306'),
    ]

    operations = [
        migrations.AlterField(
            model_name='criteriatemplate',
            name='creator',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='criteria_template_from_creator', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AlterField(
            model_name='criteriatemplate',
            name='ladder',
            field=models.SmallIntegerField(choices=[(5, '5'), (10, '10')], default=0),
        ),
    ]

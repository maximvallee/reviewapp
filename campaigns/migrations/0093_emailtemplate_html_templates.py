# Generated by Django 3.2 on 2021-09-18 00:15

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('campaigns', '0092_auto_20210917_1946'),
    ]

    operations = [
        migrations.AddField(
            model_name='emailtemplate',
            name='html_templates',
            field=models.SmallIntegerField(choices=[(1, 'template_1'), (2, 'template_2')], default=1),
        ),
    ]

# Generated by Django 3.2 on 2021-05-11 23:23

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('campaigns', '0059_auto_20210428_2049'),
    ]

    operations = [
        migrations.AddField(
            model_name='route',
            name='thumbnail',
            field=models.FileField(default='logos/anonymous.jpg', upload_to='routes'),
        ),
    ]

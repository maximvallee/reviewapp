# Generated by Django 3.2 on 2021-04-29 00:49

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('campaigns', '0058_alter_campaign_route'),
    ]

    operations = [
        migrations.AlterField(
            model_name='campaign',
            name='route',
            field=models.ManyToManyField(blank=True, related_name='campaigns_from_route', to='campaigns.Route'),
        ),
        migrations.AlterField(
            model_name='recipient',
            name='email',
            field=models.EmailField(max_length=254, unique=True),
        ),
    ]

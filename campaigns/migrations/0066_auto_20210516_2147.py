# Generated by Django 3.2 on 2021-05-17 01:47

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('campaigns', '0065_alter_introtemplate_redirect_subheader'),
    ]

    operations = [
        migrations.AddField(
            model_name='introtemplate',
            name='background_color',
            field=models.CharField(default='#F2F2F5', max_length=7),
        ),
        migrations.AddField(
            model_name='introtemplate',
            name='button_color',
            field=models.CharField(default='#00b67a', max_length=7),
        ),
        migrations.AddField(
            model_name='introtemplate',
            name='header_color',
            field=models.CharField(default='#000032', max_length=7),
        ),
        migrations.AlterField(
            model_name='introtemplate',
            name='redirect_subheader',
            field=models.TextField(max_length=96),
        ),
    ]

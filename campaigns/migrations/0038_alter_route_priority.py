# Generated by Django 3.2 on 2021-04-20 21:23

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('campaigns', '0037_rename_rank_route_priority'),
    ]

    operations = [
        migrations.AlterField(
            model_name='route',
            name='priority',
            field=models.SmallIntegerField(choices=[(1, '1'), (2, '2'), (3, '3'), (4, '4'), (5, '5')], default=0, unique=True),
        ),
    ]

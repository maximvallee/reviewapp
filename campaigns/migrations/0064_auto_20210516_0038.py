# Generated by Django 3.2 on 2021-05-16 04:38

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('campaigns', '0063_auto_20210513_1640'),
    ]

    operations = [
        migrations.AddField(
            model_name='introtemplate',
            name='redirect_header',
            field=models.CharField(default=1, max_length=48),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='introtemplate',
            name='redirect_subheader',
            field=models.CharField(default='Lorem ipsum blablabla', max_length=48),
            preserve_default=False,
        ),
    ]

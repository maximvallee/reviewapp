# Generated by Django 3.2 on 2021-05-26 20:58

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('campaigns', '0078_auto_20210526_1647'),
    ]

    operations = [
        migrations.AlterField(
            model_name='route',
            name='thumbnail',
            field=models.FileField(default='routes/default.png', upload_to='media/routes'),
        ),
    ]

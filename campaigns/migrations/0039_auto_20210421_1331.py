# Generated by Django 3.2 on 2021-04-21 17:31

from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('campaigns', '0038_alter_route_priority'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='score',
            name='evaluation',
        ),
        migrations.AddField(
            model_name='reviewrequest',
            name='created',
            field=models.DateTimeField(auto_now_add=True, default=django.utils.timezone.now),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='reviewrequest',
            name='feedback',
            field=models.TextField(blank=True, max_length=750, null=True),
        ),
        migrations.AddField(
            model_name='reviewrequest',
            name='result',
            field=models.SmallIntegerField(blank=True, choices=[(0, 'Pass'), (1, 'Fail')], null=True),
        ),
        migrations.AddField(
            model_name='reviewrequest',
            name='routedTo',
            field=models.CharField(blank=True, max_length=48, null=True),
        ),
        migrations.AddField(
            model_name='score',
            name='review_request',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, related_name='scores_from_review_request', to='campaigns.reviewrequest'),
            preserve_default=False,
        ),
        migrations.DeleteModel(
            name='Evaluation',
        ),
    ]

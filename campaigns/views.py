# from reviewproject.campaigns.models import Evaluation
from django.urls import resolve
from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from campaigns.models import *
from campaigns.forms import *
from django.db import transaction
import csv
import io
from django.shortcuts import render
from django.contrib import messages
import datetime
from campaigns.utils import send_mass_html_mail, to_snake_case, test_scores, is_email_valid, find_duplicates, hex_to_rgb, create_context_email, url_to_view
from django.template.loader import render_to_string
from html2text import html2text
from django.template import Template, Context
from django.urls import reverse
from django.contrib.auth.decorators import login_required
from django.http import Http404
from django.contrib.auth.decorators import login_required
from reviewproject.decorators import is_creator_required
from django.shortcuts import get_object_or_404
import pandas
from django.core.exceptions import ObjectDoesNotExist
from django.shortcuts import render
from django.template import RequestContext

from campaigns.choices import *

# Create your views here.


@login_required
def index(request):
    user = request.user
    context = {
        'user': user
    }

    return render(request, "campaigns/index.html", context)


"""
Campaigns
"""


@login_required
def list_campaigns(request):

    user = request.user

    context = {
        'campaigns': Campaign.objects.filter(creator=user),
        'view_name': url_to_view(request.path)
    }
    return render(request, "campaigns/campaigns.html", context)


@login_required
@is_creator_required
@transaction.atomic()
def detail_campaign(request, pk):

    user = request.user
    campaign = Campaign.objects.get(id=pk)
    review_requests = campaign.review_requests_from_campaign.all()

    if request.method == "POST":
        selected_ids = [box[9:]
                        for box in request.POST.keys() if box.startswith("selected_")]
        selected_requests = ReviewRequest.objects.filter(
            id__in=selected_ids)  # Turn ID list [23, 25] into QuerySet

        email_list = []
        message_list = []
        form = CampaignForm(request.POST, user=user)
        form.fields['email_template'].required = True
        sender = request.user

        if 'delete' in request.POST:
            selected_requests.delete()

        if 'edit' in request.POST:

            if form.is_valid():
                data = form.cleaned_data
                email_template = data['email_template']
                url_redirect = reverse('campaigns:email_template_update', kwargs={
                    'pk': email_template.id}) + '?next=/campaign/{}'.format(pk)

                return redirect(url_redirect)

            return render(request, 'campaigns/campaign.html', {'form': form, 'pk': pk, 'campaign': campaign, 'review_requests': review_requests})

        if 'test' in request.POST:

            if form.is_valid():
                data = form.cleaned_data
                email_template = data['email_template']
                html_template_url = 'campaigns/emails/{}.html'.format(
                    HTML_EMAIL_OPTIONS[email_template.html_template][1].replace(" ", "_").lower())
                recipient = request.user

                context_email = create_context_email(
                    request, email_template, recipient, sender, campaign)
                html_message = render_to_string(
                    html_template_url, context_email)
                plain_message = html2text(html_message)

                email_list.append((context_email['subject_line'], plain_message,
                                  html_message, context_email['sender_email'], [recipient.test_email]))

                # import pdb
                # pdb.set_trace()
                send_mass_html_mail(email_list)

            return render(request, 'campaigns/campaign.html', {'form': form, 'pk': pk, 'campaign': campaign, 'review_requests': review_requests})

        if 'send' in request.POST:

            if form.is_valid():
                data = form.cleaned_data
                email_template = data['email_template']
                html_template_url = 'campaigns/emails/{}.html'.format(
                    HTML_EMAIL_OPTIONS[email_template.html_template][1].replace(" ", "_").lower())

                for review_request in selected_requests:
                    recipient = review_request.recipient
                    context_email = create_context_email(
                        request, email_template, recipient, sender, campaign, review_request)

                    html_message = render_to_string(
                        html_template_url, context_email)
                    plain_message = html2text(html_message)

                    email_list.append((context_email['subject_line'], plain_message,
                                       html_message, context_email['sender_email'], [recipient.email]))

                    tuple(message_list)
                    message_list.append(Message(
                        email_template=email_template, review_request=review_request, sent=datetime.datetime.now()))

                # import pdb
                # pdb.set_trace()
                Message.objects.bulk_create(message_list)
                send_mass_html_mail(email_list, bcc=[request.user.email])

            # return redirect('campaigns:campaign_detail', pk=pk)
            return render(request, 'campaigns/campaign.html', {'form': form, 'pk': pk, 'campaign': campaign, 'review_requests': review_requests})

    context = {
        'campaign': campaign,
        'url_signup_recipient': request.build_absolute_uri(reverse('campaigns:recipient_signup', kwargs={'pk': campaign.pk})),
        'review_requests': review_requests,
        'form': CampaignForm(user=user),
        'view_name': url_to_view(request.path)
    }
    return render(request, "campaigns/campaign.html", context)


@login_required
def create_campaign(request):

    # import pdb; pdb.set_trace()
    instance = Campaign.objects.create(
        creator=request.user,
        intro_template=IntroTemplate.objects.filter(creator=request.user).get(
            is_default=True)  # Default intro template
    )

    defaultCriteria = CriteriaTemplate.objects.filter(
        creator=request.user).get(is_default=True)  # Default criteria
    instance.criteria_templates.add(defaultCriteria)

    return redirect('campaigns:campaigns_list')


@login_required
@is_creator_required
def update_campaign(request, pk):

    print(url_to_view(request.path))
    user = request.user
    campaign = Campaign.objects.get(id=pk)

    # import pdb; pdb.set_trace()
    if request.method == "POST":
        form = CampaignSettingsForm(request.POST, user=user)
        if form.is_valid():
            data = form.cleaned_data
            campaign.title = data['title']
            campaign.extra_field1_label = data['extra_field1_label']
            campaign.extra_field2_label = data['extra_field2_label']
            campaign.intro_template = data['intro_template']
            campaign.criteria_templates.set(data['criteria_templates'])
            campaign.route.set(data['route'])

            campaign.fail_url = data['fail_url']
            campaign.fail_timeout = data['fail_timeout']

            campaign.save()

            return redirect('campaigns:campaign_detail', pk=pk)

    form = CampaignSettingsForm(instance=campaign, user=user)

    context = {
        'pk': pk,
        'form': form,
    }

    return render(request, 'campaigns/campaign_setup_form.html', context)


@login_required
@is_creator_required
def delete_campaign(request, pk):
    campaign = Campaign.objects.get(id=pk)
    campaign.delete()
    return redirect('campaigns:campaigns_list')


"""
Recipients & Requests
"""


@login_required
@is_creator_required
@transaction.atomic()
def create_recipient(request, pk):

    print(url_to_view(request.path))

    campaign = Campaign.objects.get(id=pk)

    if request.method == "POST":
        form = RecipientForm(request.POST, campaign=campaign)
        if form.is_valid():
            data = form.cleaned_data
            # import pdb; pdb.set_trace()
            newRecipient = Recipient.objects.create(
                creator=campaign.creator,
                first_name=data['first_name'],
                last_name=data['last_name'],
                email=data['email']
            )

            if 'extra_field_1' in data:
                newRecipient.extra_field_1 = data['extra_field_1']
            if 'extra_field_2' in data:
                newRecipient.extra_field_2 = data['extra_field_2']
            newRecipient.save()

            ReviewRequest.objects.create(
                campaign=campaign,
                recipient=newRecipient
            )
            return redirect('campaigns:campaign_detail', pk=pk)
        else:
            context = {'form': form, 'campaign': campaign}
            return render(request, 'campaigns/review_request_form.html', context)

    form = RecipientForm(campaign=campaign)

    context = {
        'campaign': campaign,
        'form': form,
    }

    return render(request, 'campaigns/review_request_form.html', context)


@login_required
@is_creator_required
@transaction.atomic()
def upload_recipient(request, pk):

    print(url_to_view(request.path))
    campaign = Campaign.objects.get(id=pk)
    context = {'pk': pk}

    # import pdb; pdb.set_trace()

    if request.method == "POST":

        csv_file = request.FILES['file']

        """
        Check for CSV file format
        """
        if not csv_file.name.endswith('.csv'):
            messages.error(request, 'This is not a CSV file')
            return redirect('campaigns:recipient_upload', pk=pk)

        colnames = ['first_name', 'last_name',
                    'email', 'extra_field_1', 'extra_field_2']
        dataframe = pandas.read_csv(csv_file, names=colnames)

        """
        Check for empty file
        """
        if dataframe.empty:
            messages.error(request, 'File is empty')
            return redirect('campaigns:recipient_upload', pk=pk)

        headers = dataframe.iloc[0].tolist()
        data = dataframe.iloc[1:]

        first_name = data.first_name.tolist()
        last_name = data.last_name.tolist()
        emails = data.email.tolist()
        extra_field_1 = data.extra_field_1.tolist()
        extra_field_2 = data.extra_field_2.tolist()

        """
        Check for empty fields in the required columns
        """
        # import pdb; pdb.set_trace()
        if data[['first_name', 'last_name', 'email']].isnull().values.any():
            messages.error(
                request, 'Empty cell(s) in the first_name, last_name or email columns.')
            return redirect('campaigns:recipient_upload', pk=pk)

        """
        Check for empty fields in the extra columns
        """
        row_count = len(data.index)
        null_count_list = [
            data[['extra_field_1']].isnull().sum().extra_field_1,
            data[['extra_field_2']].isnull().sum().extra_field_2
        ]
        limits = range(1, row_count)
        for null_count in null_count_list:
            if null_count in limits:
                messages.error(
                    request, 'Empty cell(s) in the extra fields columns.')
                return redirect('campaigns:recipient_upload', pk=pk)

        """
        Check for invalid emails
        """
        for email in emails:
            if not is_email_valid(email):
                # import pdb; pdb.set_trace()
                messages.error(request, 'Invalid email: {}'.format(email))
                return redirect('campaigns:recipient_upload', pk=pk)

        """
        Check for duplicate emails
        """
        dupes = find_duplicates(emails)
        if dupes:
            messages.error(request, 'Duplicate emails: {}'.format(dupes))
            return redirect('campaigns:recipient_upload', pk=pk)

        for index, row_df in data.iterrows():
            row = row_df.tolist()
            # import pdb; pdb.set_trace()
            try:
                recipient = Recipient.objects.get(email=row[2])
                recipient.first_name = row[0]
                recipient.last_name = row[1]
                recipient.extra_field_1 = '' if row[3].__str__(
                ) == 'nan' else row[3]
                recipient.extra_field_2 = '' if row[4].__str__(
                ) == 'nan' else row[4]
                recipient.save()

            except ObjectDoesNotExist:
                recipient = Recipient.objects.create(
                    creator=request.user,
                    first_name=row[0],
                    last_name=row[1],
                    email=row[2],
                    extra_field_1='' if row[3].__str__() == 'nan' else row[3],
                    extra_field_2='' if row[4].__str__() == 'nan' else row[4]
                )

            ReviewRequest.objects.update_or_create(
                campaign=campaign,
                recipient=recipient
            )

            # import pdb; pdb.set_trace()
            campaign.extra_field1_label = '' if headers[3].__str__(
            ) == 'nan' else headers[3]
            campaign.extra_field2_label = '' if headers[4].__str__(
            ) == 'nan' else headers[4]
            campaign.save()

        return redirect('campaigns:campaign_detail', pk=pk)

    return render(request, "campaigns/recipient_upload_form.html", context)


@transaction.atomic()
def signup_recipient(request, pk):
    # import pdb; pdb.set_trace()

    campaign = Campaign.objects.get(id=pk)

    if request.method == "POST":
        form = RecipientForm(request.POST, campaign=campaign)
        if form.is_valid():
            data = form.cleaned_data
            newRecipient = Recipient.objects.create(
                creator=campaign.creator,
                first_name=data['first_name'],
                last_name=data['last_name'],
                email=data['email']
            )

            if 'extra_field_1' in data:
                newRecipient.extra_field_1 = data['extra_field_1']
            if 'extra_field_2' in data:
                newRecipient.extra_field_2 = data['extra_field_2']
            newRecipient.save()

            review_request = ReviewRequest.objects.create(
                campaign=campaign,
                recipient=newRecipient
            )
            return redirect('campaigns:evaluation_submit', slug=review_request.slug)
        else:
            context = {'campaign': campaign, 'form': form, }
            return render(request, 'campaigns/evaluation/registration_form.html', context)

    form = RecipientForm(campaign=campaign)

    context = {
        'campaign': campaign,
        'form': form,
    }

    return render(request, 'campaigns/evaluation/registration_form.html', context)


@login_required
@is_creator_required
def update_recipient(request, pk, rpk):

    recipient = Recipient.objects.get(id=rpk)
    campaign = Campaign.objects.get(id=pk)

    if request.method == "POST":
        # form = RecipientForm(request.POST, instance=recipient, campaign=campaign)
        form = RecipientForm(request.POST, campaign=campaign)
        if form.is_valid():
            data = form.cleaned_data
            # import pdb; pdb.set_trace()
            try:
                recipient.first_name = data['first_name']
                recipient.last_name = data['last_name']
                recipient.email = data['email']

                if 'extra_field_1' in data:
                    recipient.extra_field_1 = data['extra_field_1']
                if 'extra_field_2' in data:
                    recipient.extra_field_2 = data['extra_field_2']
                recipient.save()
            except:
                messages.error(request, 'Recipient already exists')
                return redirect('campaigns:recipient_update', pk=pk, rpk=rpk)
                print('create a new recipient and join it to this review request')

            return redirect('campaigns:campaign_detail', pk=pk)
        else:
            # import pdb; pdb.set_trace()
            context = {'form': form, 'campaign': campaign}
            return render(request, 'campaigns/review_request_form.html', context)

    data = {
        'first_name': recipient.first_name,
        'last_name': recipient.last_name,
        'email': recipient.email,
        'extra_field_1': recipient.extra_field_1,
        'extra_field_2': recipient.extra_field_2,
    }

    form = RecipientForm(data, campaign=campaign)

    context = {
        'campaign': campaign,
        'form': form,
    }

    return render(request, 'campaigns/review_request_form.html', context)


def unsubscribe_recipient(request, pk, unsub_id):
    try:
        recipient = Recipient.objects.filter(
            subscribed=True).filter(id=pk).get(unsub_id=unsub_id)
        recipient.subscribed = False
        recipient.save()
    except Recipient.DoesNotExist:
        raise Http404("Invalid recipient or unsub id")

    return render(request, 'campaigns/ubsubscribed.html', context=None)


"""
Page Templates
"""


@login_required
def list_review_page_templates(request):
    review_page_list = IntroTemplate.objects.filter(creator=request.user.id)
    context = {
        'review_page_list': review_page_list,
        'view_name': url_to_view(request.path)
    }
    return render(request, "campaigns/review_page_templates_list.html", context)


@login_required
@is_creator_required
def update_review_page_template(request, pk):

    instance = IntroTemplate.objects.get(id=pk)

    if request.method == "POST":
        form = ReviewPageTemplateForm(request.POST)
        # import pdb; pdb.set_trace()
        if form.is_valid():
            data = form.cleaned_data
            instance.subject_line = data['subject_line']
            instance.body = data['body']
            instance.redirect_heading = data['redirect_heading']
            instance.redirect_subheading = data['redirect_subheading']
            instance.comments_label = data['comments_label']
            instance.comments_placeholder = data['comments_placeholder']
            instance.thankyou_heading = data['thankyou_heading']
            instance.thankyou_subheading = data['thankyou_subheading']
            instance.navbar_color = data['navbar_color']
            instance.background_color = data['background_color']
            instance.button_color = data['button_color']
            instance.heading_color = data['heading_color']
            instance.subheading_color = data['subheading_color']
            instance.body_color = data['body_color']

            instance.save()

            return redirect('campaigns:review_page_templates_list')

    form = ReviewPageTemplateForm(instance=instance)

    context = {
        'form': form,
        'view_name': url_to_view(request.path)
    }

    return render(request, "campaigns/review_page_template_form.html", context)


@login_required
def create_review_page_template(request):
    IntroTemplate.objects.create(creator=request.user)

    return redirect('campaigns:review_page_templates_list')


@login_required
@is_creator_required
def duplicate_review_page_template(request, pk):
    instance = IntroTemplate.objects.get(id=pk)
    instance.pk = None
    instance.save()
    return redirect('campaigns:review_page_templates_list')


@login_required
@is_creator_required
def delete_review_page_template(request, pk):
    instance = IntroTemplate.objects.get(id=pk)
    instance.delete()
    return redirect('campaigns:review_page_templates_list')


"""
Review Criteria
"""


@login_required
def list_review_criteria(request):
    review_criteria_list = CriteriaTemplate.objects.filter(
        creator=request.user.id)
    context = {
        'review_criteria_list': review_criteria_list,
        'view_name': url_to_view(request.path)
    }
    return render(request, "campaigns/criteria_list.html", context)


@login_required
@is_creator_required
def update_review_criteria(request, pk):

    instance = CriteriaTemplate.objects.get(id=pk)

    if request.method == "POST":
        form = CriteriaTemplateForm(request.POST)
        # import pdb; pdb.set_trace()
        if form.is_valid():
            data = form.cleaned_data
            instance.name = data['name']
            instance.label = data['label']
            instance.ladder = data['ladder']
            instance.save()
            return redirect('campaigns:review_criteria_list')

    form = CriteriaTemplateForm(instance=instance)

    context = {
        'form': form,
        'view_name': url_to_view(request.path)
    }

    return render(request, "campaigns/criteria_form.html", context)


@login_required
def create_review_criteria(request):

    if request.method == "POST":
        form = CriteriaTemplateForm(request.POST)

        if form.is_valid():
            data = form.cleaned_data
            CriteriaTemplate.objects.create(
                creator=request.user,
                name=data['name'],
                label=data['label'],
                ladder=data['ladder'],
            )
            return redirect('campaigns:review_criteria_list')

    form = CriteriaTemplateForm()

    context = {
        'form': form,
        'view_name': url_to_view(request.path)
    }

    return render(request, "campaigns/criteria_form.html", context)


@login_required
@is_creator_required
def delete_review_criteria(request, pk):
    instance = CriteriaTemplate.objects.get(id=pk)
    instance.delete()
    return redirect('campaigns:review_criteria_list')


"""
Email Templates
"""


@login_required
def list_email_templates(request):
    email_list = EmailTemplate.objects.filter(creator=request.user.id)
    context = {
        'email_list': email_list,
        'view_name': url_to_view(request.path)
    }
    return render(request, "campaigns/email_templates_list.html", context)


@login_required
@is_creator_required
def update_email_template(request, pk):

    instance = EmailTemplate.objects.get(id=pk)

    if request.method == "POST":
        form = EmailTemplateForm(request.POST)
        # import pdb; pdb.set_trace()
        if form.is_valid():
            data = form.cleaned_data
            instance.template_name = data['template_name']
            instance.subject_line = data['subject_line']
            instance.heading = data['heading']
            instance.body = data['body']
            instance.sub_heading = data['sub_heading']
            instance.sub_body = data['sub_body']
            instance.button = data['button']
            instance.heading_color = data['heading_color']
            instance.body_color = data['body_color']
            instance.background_color = data['background_color']
            instance.button_color = data['button_color']
            instance.html_template = data['html_template']
            instance.save()
            return redirect('campaigns:email_templates_list')

    form = EmailTemplateForm(instance=instance)

    extra_field1_labels = Campaign.objects.filter(creator=request.user).values_list(
        'extra_field1_label', flat=True).exclude(extra_field1_label=None)
    extra_field2_labels = Campaign.objects.filter(creator=request.user).values_list(
        'extra_field2_label', flat=True).exclude(extra_field2_label=None)
    email_tags = extra_field1_labels.union(extra_field2_labels)

    context = {
        'email_tags': email_tags,
        'form': form,
        'view_name': url_to_view(request.path)
    }

    return render(request, "campaigns/email_template_form.html", context)


@login_required
def create_email_template(request):
    # import pdb; pdb.set_trace()

    next_url = request.GET.get('next')

    if request.method == "POST":
        form = EmailTemplateForm(request.POST)

        if form.is_valid():
            data = form.cleaned_data
            EmailTemplate.objects.create(
                creator=request.user,
                template_name=data['template_name'],
                subject_line=data['subject_line'],
                heading=data['heading'],
                body=data['body'],
                button=data['button'],
                heading_color=data['heading_color'],
                body_color=data['body_color'],
                background_color=data['background_color'],
                button_color=data['button_color'],
            )
            if next_url:
                return redirect(next_url)
            return redirect('campaigns:email_templates_list')

    form = EmailTemplateForm()

    extra_field1_labels = Campaign.objects.filter(creator=request.user).values_list(
        'extra_field1_label', flat=True).exclude(extra_field1_label=None)
    extra_field2_labels = Campaign.objects.filter(creator=request.user).values_list(
        'extra_field2_label', flat=True).exclude(extra_field2_label=None)
    email_tags = extra_field1_labels.union(extra_field2_labels)

    context = {
        'email_tags': email_tags,
        'form': form
    }

    return render(request, "campaigns/email_template_form.html", context)


@login_required
@is_creator_required
def duplicate_email_template(request, pk):
    instance = EmailTemplate.objects.get(id=pk)
    instance.pk = None
    instance.save()
    return redirect('campaigns:email_templates_list')


@login_required
@is_creator_required
def delete_email_template(request, pk):
    instance = EmailTemplate.objects.get(id=pk)
    instance.delete()
    return redirect('campaigns:email_templates_list')


"""
Routes
"""


@login_required
def list_routes(request):
    route_list = Route.objects.filter(
        creator=request.user).order_by('priority')

    # import pdb; pdb.set_trace()
    context = {
        'route_list': route_list,
        'view_name': url_to_view(request.path)
    }
    return render(request, "campaigns/routes_list.html", context)


@login_required
@is_creator_required
def update_route(request, pk):

    instance = Route.objects.get(id=pk)

    if request.method == "POST":
        form = RouteForm(request.POST, request.FILES, user=request.user)
        # import pdb; pdb.set_trace()
        if form.is_valid():
            data = form.cleaned_data
            instance.name = data['name']
            instance.priority = data['priority']
            instance.url = data['url']
            instance.url_thumbnail = data['url_thumbnail']
            instance.conditions.set(data['conditions'])
            # if 'default' not in data['thumbnail']: instance.thumbnail = data['thumbnail'] ## <<<<<< ??
            instance.save()
            return redirect('campaigns:routes_list')
        else:
            context = {'form': form}
            return render(request, "campaigns/route_form.html", context)

    form = RouteForm(instance=instance, user=request.user)
    context = {
        'form': form,
        'route': instance,
        'view_name': url_to_view(request.path)
    }

    return render(request, "campaigns/route_form.html", context)


@login_required
def create_route(request):

    if request.method == "POST":
        form = RouteForm(request.POST, request.FILES, user=request.user)
        # import pdb; pdb.set_trace()
        if form.is_valid():
            data = form.cleaned_data
            instance = Route.objects.create(
                creator=request.user,
                name=data['name'],
                priority=data['priority'],
                url=data['url'],
                url_thumbnail=data['url_thumbnail'],
                # thumbnail=data['thumbnail'] ## <<<<<< ??
            )
            instance.conditions.set(data['conditions'])
            instance.save()
            return redirect('campaigns:routes_list')
        else:
            context = {'form': form}
            return render(request, "campaigns/route_form.html", context)

    form = RouteForm(user=request.user)

    context = {
        'form': form,
        'view_name': url_to_view(request.path)
    }

    return render(request, "campaigns/route_form.html", context)


@login_required
@is_creator_required
def delete_route(request, pk):
    instance = Route.objects.get(id=pk)
    instance.delete()
    return redirect('campaigns:routes_list')


"""
Conditions
"""


@login_required
def list_conditions(request):
    condition_list = Condition.objects.filter(creator=request.user)

    # import pdb; pdb.set_trace()
    context = {
        'condition_list': condition_list,
        'view_name': url_to_view(request.path)
    }
    return render(request, "campaigns/conditions_list.html", context)


@login_required
def create_condition(request):

    if request.method == "POST":
        form = ConditionForm(request.POST)
        if form.is_valid():
            data = form.cleaned_data
            Condition.objects.create(
                creator=request.user,
                input_field=data['input_field'],
                comparison_operator=data['comparison_operator'],
                passing_value=data['passing_value']
            )
            return redirect('campaigns:conditions_list')

    form = ConditionForm()

    context = {
        'form': form,
        'view_name': url_to_view(request.path)
    }

    return render(request, 'campaigns/condition_form.html', context)


@login_required
@is_creator_required
def update_condition(request, pk):

    instance = Condition.objects.get(id=pk)

    if request.method == "POST":
        form = ConditionForm(request.POST)
        # import pdb; pdb.set_trace()
        if form.is_valid():
            data = form.cleaned_data
            instance.input_field = data['input_field']
            instance.comparison_operator = data['comparison_operator']
            instance.passing_value = data['passing_value']
            instance.save()
            return redirect('campaigns:conditions_list')

    form = ConditionForm(instance=instance)

    context = {
        'form': form,
        'view_name': url_to_view(request.path)
    }

    return render(request, "campaigns/condition_form.html", context)


@login_required
@is_creator_required
def delete_condition(request, pk):
    instance = Condition.objects.get(id=pk)
    instance.delete()
    return redirect('campaigns:conditions_list')


"""
Evaluations
"""


def email_evaluation(request, slug):

    review_request = ReviewRequest.objects.get(slug=slug)
    campaign = review_request.campaign

    context = {
        'slug': slug,
        'campaign': campaign,
        'criteria': campaign.criteria_templates.first()
    }

    return render(request, "campaigns/evaluation/email_form.html", context)


def email_recapture(request, slug, rpk):
    review_request = ReviewRequest.objects.get(slug=slug)
    route = Route.objects.get(id=rpk)
    campaign = review_request.campaign
    evaluations = review_request.evaluations_from_review_request.all()

    # If the Review Request has an existing evaluation, take the last one
    if evaluations:
        last_evaluation = evaluations.reverse().first()
        last_evaluation.routedTo = route.name
        last_evaluation.save()
    # If not, create one
    else:
        Evaluation.objects.create(
            review_request=review_request,
            routedTo=route.name
        )

    ### SEND EMAIL NOTIFICATION TO ADMIN ###
    try:

        html_template_url = 'campaigns/emails/incoming_review.html'
        sender = campaign.creator
        url_campaign = request.build_absolute_uri(
            reverse('campaigns:campaign_detail', kwargs={'pk': campaign.id}))

        context_email = {
            'sender_name': sender.first_name,
            'url_campaign': url_campaign,
            'recipient_name': review_request.recipient.first_name
        }

        html_message = render_to_string(
            html_template_url, context_email)
        plain_message = html2text(html_message)

        email_list = [('You received a new review', plain_message, html_message,
                       sender.email, [sender.email])]

        send_mass_html_mail(email_list)

    except:
        pass

    return redirect(route.url)


def email_router(request, slug, rating):

    review_request = ReviewRequest.objects.get(slug=slug)
    campaign = review_request.campaign
    # import pdb
    # pdb.set_trace()

    evaluation = Evaluation.objects.create(
        review_request=review_request,
    )

    score = Score.objects.create(
        value=rating,
        # ladder=criteria.ladder,
        criteria=campaign.criteria_templates.all().first(),
        evaluation=evaluation
    )

    context = {
        'slug': slug,
        'campaign': campaign,
    }

    ### SEND EMAIL NOTIFICATION TO ADMIN ###
    try:

        html_template_url = 'campaigns/emails/incoming_review.html'
        sender = campaign.creator
        url_campaign = request.build_absolute_uri(
            reverse('campaigns:campaign_detail', kwargs={'pk': campaign.id}))

        context_email = {
            'sender_name': sender.first_name,
            'url_campaign': url_campaign,
            'recipient_name': review_request.recipient.first_name
        }

        html_message = render_to_string(
            html_template_url, context_email)
        plain_message = html2text(html_message)

        email_list = [('You received a new review', plain_message, html_message,
                       sender.email, [sender.email])]

        send_mass_html_mail(email_list)

    except:
        pass
    # import pdb
    # pdb.set_trace()

    ### DISPATCH ###
    # import pdb
    # pdb.set_trace()
    routes = campaign.route.all().order_by('priority')
    for route in routes:
        results = {
            'conditions': [],
            'values': []
        }

        for condition in route.conditions.all():
            results['conditions'].append(condition)

            # Testing other input fields
            if condition.input_field == 0:
                isSubstring = condition.passing_value in review_request.recipient.email
                results['values'].append(isSubstring)

            # Testing evaluation scores
            # import pdb; pdb.set_trace()
            if condition.input_field > 0:
                isSuccessful = test_scores(
                    evaluation,
                    condition.input_field,
                    condition.comparison_operator,
                    condition.passing_value,
                )
                results['values'].append(isSuccessful)

        print(route, results)

        # EXTERNAL ROUTING ROUTING
        if not False in results['values']:
            # import pdb
            # pdb.set_trace()
            altroutes = routes.exclude(id=route.id)

            context = {'route': route,
                       'altroutes': altroutes,
                       'review_request': review_request,
                       'campaign': review_request.campaign,
                       'evaluation': evaluation,
                       'private_review_url': request.build_absolute_uri(reverse('campaigns:comments_submit', kwargs={'slug': review_request.slug}))

                       }
            return render(request, 'campaigns/evaluation/post_redirect.html', context)

    # INTERNAL ROUTING ROUTING
    evaluation.result = 1  # Fail
    evaluation.routedTo = 'Internal'
    evaluation.save()
    return redirect('campaigns:comments_submit', slug=review_request.slug)


@transaction.atomic()
def submit_evaluation(request, slug):
    # import pdb
    # pdb.set_trace()
    review_request = ReviewRequest.objects.get(slug=slug)
    campaign = review_request.campaign
    criteria_list = campaign.criteria_templates.all()
    now = datetime.datetime.now()
    scores = []

    if request.method == "POST":
        form = EvaluationForm(request.POST, review=review_request)
        if form.is_valid():
            data = form.cleaned_data

            evaluation = Evaluation.objects.create(
                review_request=review_request,
            )
            # import pdb; pdb.set_trace()
            for index, criteria in enumerate(criteria_list):
                # import pdb
                # pdb.set_trace()
                score = Score.objects.create(
                    value=data['criteria-{}'.format(index)],
                    # ladder=criteria.ladder,
                    criteria=criteria_list[index],
                    evaluation=evaluation
                )

            ### DISPATCH ###
            # import pdb; pdb.set_trace()
            routes = campaign.route.all().order_by('priority')
            for route in routes:
                results = {
                    'conditions': [],
                    'values': []
                }

                for condition in route.conditions.all():
                    results['conditions'].append(condition)

                    # Testing other input fields
                    if condition.input_field == 0:
                        isSubstring = condition.passing_value in review_request.recipient.email
                        results['values'].append(isSubstring)

                    # Testing evaluation scores
                    # import pdb; pdb.set_trace()
                    if condition.input_field > 0:
                        isSuccessful = test_scores(
                            evaluation,
                            condition.input_field,
                            condition.comparison_operator,
                            condition.passing_value,
                        )
                        results['values'].append(isSuccessful)

                print(route, results)

                # EXTERNAL ROUTING ROUTING
                if not False in results['values']:
                    # import pdb; pdb.set_trace()
                    altroutes = routes.exclude(id=route.id)

                    context = {'route': route,
                               'altroutes': altroutes,
                               'review_request': review_request,
                               'campaign': review_request.campaign,
                               'evaluation': evaluation,
                               'private_review_url': request.build_absolute_uri(reverse('campaigns:comments_submit', kwargs={'slug': review_request.slug}))

                               }
                    return render(request, 'campaigns/evaluation/post_redirect.html', context)

            # INTERNAL ROUTING ROUTING
            evaluation.result = 1  # Fail
            evaluation.routedTo = 'Internal'
            evaluation.save()
            return redirect('campaigns:comments_submit', slug=review_request.slug)

    form = EvaluationForm(review=review_request)

    # import pdb; pdb.set_trace()
    context = {
        'review_request': review_request,
        'campaign': campaign,
        'form': form,
    }

    return render(request, 'campaigns/evaluation/evaluation_form.html', context)


def submit_comments(request, slug):
    review_request = ReviewRequest.objects.get(slug=slug)
    campaign = review_request.campaign
    evaluations = review_request.evaluations_from_review_request.all()

    # import pdb
    # pdb.set_trace()
    # If the Review Request has an existing evaluation, take the last one
    if evaluations:
        evaluation = evaluations.reverse().first()
    # If not, create one
    else:
        evaluation = Evaluation.objects.create(
            review_request=review_request,
        )

    if request.method == "POST":
        form = CommentsForm(request.POST, campaign=campaign)
        if form.is_valid():
            data = form.cleaned_data
            evaluation.comments = data['comments']
            evaluation.routedTo = 'Internal'
            evaluation.save()

            return redirect('campaigns:thank_you', slug=slug)

    form = CommentsForm(campaign=campaign)

    context = {
        'form': form,
        'review_request': review_request,
        'campaign': campaign,
    }

    return render(request, 'campaigns/evaluation/comments_form.html', context)


def post_redirect(request, epk, rpk):
    # import pdb; pdb.set_trace()
    route = Route.objects.get(id=rpk)
    evaluation = Evaluation.objects.get(id=epk)
    # evaluation.result = 0 # Pass
    evaluation.routedTo = route.name
    evaluation.save()

    return redirect(route.url)


def thank_you(request, slug):
    review_request = ReviewRequest.objects.get(slug=slug)
    campaign = review_request.campaign
    fail_url = campaign.fail_url
    fail_timeout = campaign.fail_timeout

    context = {
        'review_request': review_request,
        'campaign': campaign,
        'redirect': fail_url,
        'timeout': fail_timeout
    }
    if fail_url and not fail_timeout:
        return redirect(fail_url)
    return render(request, 'campaigns/evaluation/thank_you.html', context)


"""
Exceptions
"""


def handler404(request, exception, template_name="campaigns/exceptions/404.html"):
    response = render(request, template_name)
    response.status_code = 404
    return response


def handler500(request, template_name="campaigns/exceptions/500.html"):
    response = render(request, template_name)
    response.status_code = 500
    return response

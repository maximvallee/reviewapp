
from django.contrib import admin
from django.urls import path, include
from campaigns import views

app_name = 'campaigns'

urlpatterns = [
    path('', views.index, name='index'),

    path('campaigns', views.list_campaigns, name='campaigns_list'),
    path('campaign/<int:pk>', views.detail_campaign, name='campaign_detail'),
    path('campaign/create', views.create_campaign, name='campaign_create'),
    path('campaign/<int:pk>/settings',
         views.update_campaign, name='campaign_settings'),
    path('campaign/<int:pk>/delete', views.delete_campaign, name='campaign_delete'),


    path('campaign/<int:pk>/recipient/create',
         views.create_recipient, name='recipient_create'),
    path('campaign/<int:pk>/recipient/upload',
         views.upload_recipient, name='recipient_upload'),
    path('campaign/<int:pk>/recipient/<int:rpk>/update',
         views.update_recipient, name='recipient_update'),
    path('campaign/<int:pk>/signup',
         views.signup_recipient, name='recipient_signup'),
    path('recipient/<int:pk>/unsubscribe/<str:unsub_id>',
         views.unsubscribe_recipient, name='ubsubscribe'),


    path('settings/email-templates', views.list_email_templates,
         name='email_templates_list'),
    path('settings/email-template/create',
         views.create_email_template, name='email_template_create'),
    path('settings/email-template/<int:pk>/update',
         views.update_email_template, name='email_template_update'),
    path('settings/email-template/<int:pk>/duplicate',
         views.duplicate_email_template, name='email_template_duplicate'),
    path('settings/email-template/<int:pk>/delete',
         views.delete_email_template, name='email_template_delete'),


    path('settings/review-pages', views.list_review_page_templates,
         name='review_page_templates_list'),
    path('settings/review-page/<int:pk>/update',
         views.update_review_page_template, name='review_page_template_update'),
    path('settings/review-page/create', views.create_review_page_template,
         name='review_page_template_create'),
    path('settings/review-template/<int:pk>/duplicate',
         views.duplicate_review_page_template, name='review_page_template_duplicate'),
    path('settings/review-page/<int:pk>/delete',
         views.delete_review_page_template, name='review_page_template_delete'),


    path('settings/routing', views.list_routes, name='routes_list'),
    path('settings/routing/create', views.create_route, name='route_create'),
    path('settings/routing/<int:pk>/update',
         views.update_route, name='route_update'),
    path('settings/routing/<int:pk>/delete',
         views.delete_route, name='route_delete'),


    path('settings/conditions', views.list_conditions, name='conditions_list'),
    path('settings/condition/create',
         views.create_condition, name='condition_create'),
    path('settings/condition/<int:pk>/delete',
         views.delete_condition, name='condition_delete'),
    path('settings/condition/<int:pk>/update',
         views.update_condition, name='condition_update'),


    path('settings/criteria', views.list_review_criteria,
         name='review_criteria_list'),
    path('settings/criteria/create', views.create_review_criteria,
         name='review_criteria_create'),
    path('settings/criteria/<int:pk>/update',
         views.update_review_criteria, name='review_criteria_update'),
    path('settings/criteria/<int:pk>/delete',
         views.delete_review_criteria, name='review_criteria_delete'),


    path('evaluation/<int:epk>/redirect/route/<int:rpk>',
         views.post_redirect, name='redirect'),
    path('evaluation/<str:slug>', views.submit_evaluation,
         name='evaluation_submit'),
    path('evaluation/<str:slug>/comments',
         views.submit_comments, name='comments_submit'),
    path('evaluation/<str:slug>/received', views.thank_you, name='thank_you'),


    path('evaluation/email/<str:slug>',
         views.email_evaluation, name='evaluation_email'),
    path('evaluation/email/<str:slug>/<int:rating>',
         views.email_router, name='email_router'),
    path('evaluation/email/<str:slug>/redirect/route/<int:rpk>',
         views.email_recapture, name='recapture_email'),

]

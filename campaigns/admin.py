from django.contrib import admin
from accounts.models import CustomUser
from campaigns.models import (EmailTemplate, IntroTemplate,
    CriteriaTemplate, Campaign, ReviewRequest, Recipient,
    Score, Condition, Route, Message, Evaluation
)
import re

# # Register your models here.
class EmailTemplateAdmin(admin.ModelAdmin):
    list_display = ['id', 'subject_line', 'creator', 'is_default']
    readonly_fields = ['is_default']

class IntroTemplateAdmin(admin.ModelAdmin):
    list_display = ['id', 'subject_line', 'creator' ]

class CriteriaTemplateAdmin(admin.ModelAdmin):
    list_display = ['id', 'label', 'creator']

class CampaignAdmin(admin.ModelAdmin):
    list_display = ['id', 'title', 'creator']

class ScoreAdmin(admin.ModelAdmin):
    list_display = ['id', 'value', 'criteria']

class EvaluationAdmin(admin.ModelAdmin):
    # list_display = ['id',]
    readonly_fields = ["routedTo"]

class RouteAdmin(admin.ModelAdmin):
    list_display = ['id', 'priority', 'name']

class RecipientAdmin(admin.ModelAdmin):
    list_display = ['id', 'subscribed', 'first_name', 'last_name']



admin.site.register(EmailTemplate, EmailTemplateAdmin)
admin.site.register(IntroTemplate, IntroTemplateAdmin)
admin.site.register(CriteriaTemplate, CriteriaTemplateAdmin)
admin.site.register(Campaign, CampaignAdmin)
admin.site.register(ReviewRequest)
admin.site.register(Recipient, RecipientAdmin)
admin.site.register(Score, ScoreAdmin)
admin.site.register(Condition)
admin.site.register(Route, RouteAdmin)
admin.site.register(Evaluation, EvaluationAdmin)
admin.site.register(Message)
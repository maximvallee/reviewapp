from django.db import models
from accounts.models import CustomUser
from campaigns.choices import *
from reviewproject.defaults import *
from django.dispatch import receiver
from django.db.models import Q
from django.db.models.signals import post_save, post_delete, pre_save
import uuid
from django.db import transaction
import secrets
from campaigns.utils import hex_to_rgb, darken_color


import re
import publicsuffix
import urllib.parse


class EmailTemplate(models.Model):
    creator = models.ForeignKey(
        CustomUser, on_delete=models.CASCADE, related_name='email_templates_from_creator')
    template_name = models.CharField(
        max_length=48, default=EMAIL_NAME_DEFAULT)
    subject_line = models.CharField(
        max_length=48, default=EMAIL_SUBJECT_DEFAULT)
    heading = models.CharField(max_length=48, default=EMAIL_HEADING_DEFAULT)
    body = models.TextField(max_length=750, default=EMAIL_BODY_DEFAULT)
    sub_heading = models.CharField(
        max_length=48, default=EMAIL_SUB_HEADING_DEFAULT)
    sub_body = models.TextField(max_length=750, default=EMAIL_SUB_BODY_DEFAULT)
    button = models.CharField(max_length=24, default=EMAIL_BUTTON_DEFAULT)
    heading_color = models.CharField(
        max_length=7, default=HEADING_COLOR_DEFAULT)
    body_color = models.CharField(max_length=7, default=BODY_COLOR_DEFAULT)
    background_color = models.CharField(
        max_length=7, default=BACKGROUND_COLOR_DEFAULT)
    button_color = models.CharField(max_length=7, default=BUTTON_COLOR_DEFAULT)
    html_template = models.SmallIntegerField(
        choices=HTML_EMAIL_OPTIONS, default=0)
    is_default = models.BooleanField(default=False)

    def __str__(self):
        return str('{} ({})').format(self.template_name, self.creator)


class IntroTemplate(models.Model):
    creator = models.ForeignKey(
        CustomUser, on_delete=models.CASCADE, related_name='intro_templates_from_creator')
    subject_line = models.CharField(
        max_length=24, default=EVALUATION_HEADING_DEFAULT)
    body = models.TextField(
        max_length=750, default=EVALUATION_SUBHEADING_DEFAULT)
    comments_label = models.CharField(
        max_length=48, default=COMMENTS_HEADING_DEFAULT)
    comments_placeholder = models.TextField(
        max_length=250, default=COMMENTS_PLACEHOLDER_DEFAULT)
    redirect_heading = models.CharField(
        max_length=48, default=REDIRECT_HEADING_DEFAULT)
    redirect_subheading = models.TextField(
        max_length=200, default=REDIRECT_SUBHEADING_DEFAULT)
    thankyou_heading = models.CharField(
        max_length=48, default=THANKYOU_HEADING_DEFAULT)
    thankyou_subheading = models.TextField(
        max_length=150, default=THANKYOU_SUBHEADING_DEFAULT)
    navbar_color = models.CharField(max_length=7, default=NAVBAR_COLOR_DEFAULT)
    heading_color = models.CharField(
        max_length=7, default=HEADING_COLOR_DEFAULT)
    subheading_color = models.CharField(
        max_length=7, default=SUBHEADING_COLOR_DEFAULT)
    body_color = models.CharField(max_length=7, default=BODY_COLOR_DEFAULT)
    background_color = models.CharField(
        max_length=7, default=BACKGROUND_COLOR_DEFAULT)
    button_color = models.CharField(max_length=7, default=BUTTON_COLOR_DEFAULT)
    is_default = models.BooleanField(default=False)

    def __str__(self):
        return str('{} ({})').format(self.subject_line, self.creator)

    def hover_button_color(self):
        r, g, b = hex_to_rgb(self.button_color)
        return darken_color(r, g, b, 0.15)


class CriteriaTemplate(models.Model):
    creator = models.ForeignKey(
        CustomUser, on_delete=models.CASCADE, related_name='criteria_templates_from_creator')
    name = models.CharField(max_length=30, default="Quality")
    label = models.CharField(max_length=120, default=CRITERIA_LABEL_TEMPLATE)
    ladder = models.SmallIntegerField(choices=SCALE_OPTIONS, default=5)
    is_default = models.BooleanField(default=False)

    def __str__(self):
        return str('{} ({})').format(self.label, self.creator)


class Recipient(models.Model):
    subscribed = models.BooleanField(default=True)
    creator = models.ForeignKey(
        CustomUser, on_delete=models.CASCADE, related_name='recipients_from_creator')
    first_name = models.CharField(max_length=48)
    last_name = models.CharField(max_length=48)
    email = models.EmailField()
    extra_field_1 = models.CharField(max_length=48, null=True, blank=True)
    extra_field_2 = models.CharField(max_length=48, null=True, blank=True)
    unsub_id = models.CharField(max_length=48, unique=True)

    """
    auto_create_unsub_hash_with_recipient() signal must be one single transaction
    """
    @transaction.atomic()
    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)

    def __str__(self):
        return str('{} {}').format(self.first_name, self.last_name)


class Condition(models.Model):
    creator = models.ForeignKey(
        CustomUser, on_delete=models.CASCADE, related_name='conditions_from_creator')
    input_field = models.SmallIntegerField(choices=INPUT_FIELDS)
    comparison_operator = models.SmallIntegerField(
        choices=COMPARISON_OPERATORS, default=0)
    passing_value = models.CharField(max_length=48)

    def __str__(self):
        return str('{} {} {}').format(self.get_input_field_display(), self.get_comparison_operator_display(), self.passing_value)


class Route(models.Model):
    creator = models.ForeignKey(
        CustomUser, on_delete=models.CASCADE, related_name='routes_from_creator')
    name = models.CharField(max_length=48)
    conditions = models.ManyToManyField(
        Condition, related_name='routes_from_condition', blank=True)
    priority = models.SmallIntegerField(choices=PRIORITY_LEVEL, default=0)
    url = models.URLField()
    url_thumbnail = models.URLField(max_length=2000)
    thumbnail = models.FileField(upload_to='media/routes')

    def clean_website_url(self):
        # import pdb; pdb.set_trace()
        url_clean = ''
        if self.website:
            url = re.compile(r"https?://(www\.)?")
            url_clean = "www." + url.sub('', self.website).strip().strip('/')
        return url_clean

    def __str__(self):
        return self.name


class Campaign(models.Model):
    creator = models.ForeignKey(
        CustomUser, on_delete=models.CASCADE, related_name='campaigns_from_creator')
    title = models.CharField(max_length=48, default='New campaign')
    extra_field1_label = models.CharField(max_length=48, null=True, blank=True)
    extra_field2_label = models.CharField(max_length=48, null=True, blank=True)
    criteria_templates = models.ManyToManyField(
        CriteriaTemplate, related_name='campaigns_from_criteria')
    intro_template = models.ForeignKey(
        IntroTemplate, on_delete=models.CASCADE, related_name='campaigns_from_intro_template')
    route = models.ManyToManyField(
        Route, related_name='campaigns_from_route', blank=True)
    fail_url = models.URLField(null=True, blank=True)
    fail_timeout = models.SmallIntegerField(
        choices=TIMEOUT_OPTIONS, null=True, blank=True)

    @property
    def completion_rate(self):
        # import pdb
        # pdb.set_trace()
        review_requests = self.review_requests_from_campaign.all().count()
        has_comments = self.review_requests_from_campaign.all().filter(
            ~Q(evaluations_from_review_request__comments=None)).count()
        routed_externally = self.review_requests_from_campaign.all().filter(
            ~Q(Q(evaluations_from_review_request__routedTo="Internal") | Q(evaluations_from_review_request__routedTo=None))).count()
        clicks = has_comments + routed_externally
        return round(clicks/review_requests * 100)

    @property
    def success_count(self):
        return self.review_requests_from_campaign.all().filter(evaluations_from_review_request__result=0).count()

    @property
    def failure_count(self):
        return self.review_requests_from_campaign.all().filter(evaluations_from_review_request__result=1).count()

    @property
    def click_rate(self):

        evaluations = Evaluation.objects.filter(
            review_request__campaign__id=self.id).count()

        requests = self.review_requests_from_campaign.all().count()

        return round(evaluations/requests * 100)

    def __str__(self):
        return str('Campaign {}').format(self.id)


class ReviewRequest(models.Model):
    campaign = models.ForeignKey(
        Campaign, on_delete=models.CASCADE, related_name='review_requests_from_campaign')
    recipient = models.ForeignKey(
        Recipient, on_delete=models.CASCADE, related_name='review_requests_from_recipient')
    slug = models.CharField(max_length=250, unique=True)

    def most_recent_message(self):
        messages = self.messages_from_review_request.all().order_by('-sent')
        if messages:
            message = messages[0]
            return message
        return None

    def number_of_messages(self):
        messages = self.messages_from_review_request.all()
        return messages.count()

    def status(self):

        clicked = self.evaluations_from_review_request.all().count() > 0
        has_comments = False
        routed_externally = False
        if clicked:
            has_comments = self.evaluations_from_review_request.all().order_by(
                '-id')[0].comments != None
            routed_externally = not (self.evaluations_from_review_request.all().order_by(
                '-id')[0].routedTo == 'Internal' or self.evaluations_from_review_request.all().order_by('-id')[0].routedTo == None)

        sent = self.messages_from_review_request.all()
        subscribed = self.recipient.subscribed

        # import pdb
        # pdb.set_trace()
        if not subscribed:
            return 'Unsubbed'
        elif has_comments or routed_externally:
            return 'Completed'
        elif clicked:
            return 'Clicked'
        elif sent:
            return 'Sent'
        return 'Unsent'

    def __str__(self):
        return str("{} ({})").format(self.recipient, self.campaign)


class Message(models.Model):
    email_template = models.ForeignKey(
        EmailTemplate, on_delete=models.CASCADE, related_name='messages_from_email_template')
    review_request = models.ForeignKey(
        ReviewRequest, on_delete=models.CASCADE, related_name='messages_from_review_request')
    sent = models.DateTimeField(null=True, blank=True)


class Evaluation(models.Model):
    review_request = models.ForeignKey(
        ReviewRequest, on_delete=models.CASCADE, related_name='evaluations_from_review_request')
    created = models.DateTimeField(auto_now_add=True)
    result = models.SmallIntegerField(choices=RATING_OPTIONS, default=0)
    comments = models.TextField(max_length=750, null=True, blank=True)
    routedTo = models.CharField(max_length=48, null=True, blank=True)


class Score(models.Model):
    value = models.IntegerField()
    # ladder = models.IntegerField(default=4)
    criteria = models.ForeignKey(
        CriteriaTemplate, on_delete=models.CASCADE, related_name='scores_from_criteria')
    evaluation = models.ForeignKey(
        Evaluation, on_delete=models.CASCADE, related_name='scores_from_evaluation')


"""
Auto-create default intro, email and criteria templates when
a user is created
"""


@receiver(post_save, sender=CustomUser)
def auto_create_templates_with_user(sender, instance, **kwargs):
    # import pdb; pdb.set_trace()
    if kwargs['created']:
        EmailTemplate.objects.create(
            creator=instance,
            is_default=True
        )
        IntroTemplate.objects.create(
            creator=instance,
            is_default=True
        )
        CriteriaTemplate.objects.create(
            creator=instance,
            is_default=True
        )

        instance.test_email = instance.email
        instance.save()


"""
Auto-add existing routes to new campaign when a campaign is created
"""


@receiver(post_save, sender=Campaign)
def auto_add_existing_routes_with_campaign(sender, instance, **kwargs):
    if kwargs['created']:
        # import pdb; pdb.set_trace()
        routes = Route.objects.filter(creator=instance.creator)
        instance.route.set(routes)
        instance.save()


"""
Auto-add routes to existing campaign when new routes created
"""


@receiver(post_save, sender=Route)
def auto_add_new_routes_with_campaign(sender, instance, **kwargs):
    if kwargs['created']:
        # import pdb; pdb.set_trace()
        existing_campaigns = Campaign.objects.filter(creator=instance.creator)
        routes = Route.objects.filter(creator=instance.creator)
        for campaign in existing_campaigns:
            campaign.route.set(routes)
            campaign.save()


"""
Creates a hash speficic to each recipient for the unsub link
"""


@receiver(post_save, sender=Recipient)
def auto_create_unsub_hash_with_recipient(sender, instance, **kwargs):
    if kwargs['created']:
        # import pdb; pdb.set_trace()
        unique_id = str(uuid.uuid4())
        instance.unsub_id = unique_id
        instance.save()


"""
Creates a url slug for the review request
"""


@receiver(post_save, sender=ReviewRequest)
def auto_create_slug_with_review_request(sender, instance, **kwargs):
    if kwargs['created']:
        # import pdb; pdb.set_trace()
        unique_id = secrets.token_urlsafe(11)
        instance.slug = unique_id
        instance.save()

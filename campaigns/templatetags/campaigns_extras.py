from django import template
from campaigns.utils import to_snake_case

register = template.Library()

"""Converts a string snake_case"""
@register.filter
def snake_case(value):
    return to_snake_case(value)

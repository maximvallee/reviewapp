from django import forms
from accounts.models import *
from campaigns.models import *
from accounts.models import *
from django.forms.widgets import TextInput


class RecipientForm(forms.Form):
    first_name = forms.CharField(label=False, widget=forms.TextInput(attrs={'placeholder': 'First name'}))
    last_name = forms.CharField(label=False, widget=forms.TextInput(attrs={'placeholder': 'Last name'}))
    email = forms.EmailField(label=False, widget=forms.TextInput(attrs={'placeholder': 'Email'}))

    def __init__(self, *args, **kwargs):
        
        self.campaign = kwargs.pop('campaign',None)
        # import pdb; pdb.set_trace()
        super(RecipientForm, self).__init__(*args, **kwargs)

        self.fields['extra_field_1'] = forms.CharField(required=True, label=False)
        self.fields['extra_field_1'].widget.attrs['placeholder'] = self.campaign.extra_field1_label

        self.fields['extra_field_2'] = forms.CharField(required=True, label=False)
        self.fields['extra_field_2'].widget.attrs['placeholder'] = self.campaign.extra_field2_label

        if not self.campaign.extra_field1_label:
            del self.fields['extra_field_1']
        if not self.campaign.extra_field2_label:
            del self.fields['extra_field_2']

    
    

class CampaignSettingsForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        # import pdb; pdb.set_trace()
        self.user = kwargs.pop('user',None)
        super(CampaignSettingsForm, self).__init__(*args, **kwargs)
        criteria_list = CriteriaTemplate.objects.filter(creator=self.user)
        page_list = IntroTemplate.objects.filter(creator=self.user)
        route_list = Route.objects.filter(creator=self.user)

        self.fields['criteria_templates'] = forms.ModelMultipleChoiceField(queryset=criteria_list, label='Evaluation criteria')
        self.fields['intro_template'] = forms.ModelChoiceField(queryset=page_list, label='Landing template')
        self.fields['route'] = forms.ModelMultipleChoiceField(queryset=route_list, label='Routes', required=False)

    class Meta():
        model = Campaign
        exclude = ('creator',)


class EvaluationForm(forms.Form):

    def __init__(self, *args, **kwargs):
        # import pdb; pdb.set_trace()
        self.review = kwargs.pop('review',None)
        super(EvaluationForm, self).__init__(*args, **kwargs)

        campaign = self.review.campaign
        criteria_list = campaign.criteria_templates.all()

        for index, criteria in enumerate(criteria_list):
            label = criteria.label
            scale = criteria.ladder
            # top_rating = criteria.top_rating
            # low_rating = criteria.low_rating

            # CHOICES = [(str(j),str(j)) for j in range(1, scale)]
            # CHOICES.insert(0, (0,low_rating))
            # CHOICES.insert(len(CHOICES), (len(CHOICES),top_rating))


            CHOICES = [(str(j),'★') for j in range(1, scale)]
            # CHOICES.insert(0, (0,low_rating))
            # CHOICES.insert(len(CHOICES), (len(CHOICES),top_rating))
            # import pdb; pdb.set_trace()

            self.fields['criteria-{}'.format(index)] = forms.ChoiceField(choices=CHOICES, label=label, widget=forms.RadioSelect (attrs={'class':'stars'}))

class CampaignForm(forms.Form):

    def __init__(self, *args, **kwargs):
        # import pdb; pdb.set_trace()
        self.user = kwargs.pop('user',None)
        super(CampaignForm, self).__init__(*args, **kwargs)
        email_templates = EmailTemplate.objects.filter(creator=self.user)
        self.fields['email_template'] = forms.ModelChoiceField(queryset=email_templates, required=False, label="", empty_label='Select an email template...')


class EmailTemplateForm(forms.ModelForm):

    class Meta:
        model = EmailTemplate
        exclude = ('creator','is_default')

        widgets = {
            'heading_color': TextInput(attrs={'type': 'color'}),
            'body_color': TextInput(attrs={'type': 'color'}),
            'background_color': TextInput(attrs={'type': 'color'}),
            'button_color': TextInput(attrs={'type': 'color'}),
        }



class ReviewPageTemplateForm(forms.ModelForm):

    class Meta:
        model = IntroTemplate
        exclude = ('creator','is_default')

        widgets = {
            'navbar_color': TextInput(attrs={'type': 'color'}),
            'heading_color': TextInput(attrs={'type': 'color'}),
            'subheading_color': TextInput(attrs={'type': 'color'}),
            'body_color': TextInput(attrs={'type': 'color'}),
            'background_color': TextInput(attrs={'type': 'color'}),
            'button_color': TextInput(attrs={'type': 'color'}),
        }


class CriteriaTemplateForm(forms.ModelForm):

    class Meta:
        model = CriteriaTemplate
        exclude = ('creator','is_default')

class RouteForm(forms.ModelForm):
    # url_thumbnail = forms.CharField(widget = forms.HiddenInput(), required = False)
    url_thumbnail = forms.CharField(widget=forms.TextInput(attrs={'class': 'hide'}))

    def __init__(self, *args, **kwargs):
        # import pdb; pdb.set_trace()
        self.user = kwargs.pop('user',None)
        super(RouteForm, self).__init__(*args, **kwargs)
        condition_list = Condition.objects.filter(creator=self.user)

        self.fields['conditions'] = forms.ModelMultipleChoiceField(queryset=condition_list, label='Conditions')

    class Meta:
        model = Route
        exclude = ('creator', 'thumbnail')


class ConditionForm(forms.ModelForm):

    class Meta:
        model = Condition
        exclude = ('creator',)

class CommentsForm(forms.Form):
    def __init__(self, *args, **kwargs):
        # import pdb; pdb.set_trace()
        self.campaign = kwargs.pop('campaign',None)
        super(CommentsForm, self).__init__(*args, **kwargs)
        label = self.campaign.intro_template.comments_label
        placeholder = self.campaign.intro_template.comments_placeholder

        self.fields['comments'] = forms.CharField(widget=forms.Textarea(attrs={
            'placeholder': placeholder
            }), 
            label=label
        )

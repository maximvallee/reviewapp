"""reviewproject URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include

import allauth ### REQUIRED FOR ALLAUTH
from django.conf import settings ### REQUIRED FOR STATIC UPLOADS
from django.conf.urls.static import static ### REQUIRED FOR STATIC UPLOADS
from django.conf.urls import handler400, handler500 ### REQUIRED FOR ERROR HANDLING

urlpatterns = [
    path('', include("campaigns.urls", namespace="campaigns")),
    path('accounts/', include("accounts.urls", namespace="accounts")),
    path('accounts/', include('allauth.urls')), ### REQUIRED FOR ALLAUTH: AUTOMATIC ACCESS TO ALL VIEWS
    path('admin/', admin.site.urls),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT) ### REQUIRED TO VIEW UPLOADED STATIC FILES

handler404 = 'campaigns.views.handler404'
handler500 = 'campaigns.views.handler500'
from django.core.exceptions import PermissionDenied
from accounts.models import CustomUser
from campaigns.models import *
from django.shortcuts import render, redirect
from django.core.exceptions import ObjectDoesNotExist
from django.urls import resolve
from django.shortcuts import get_object_or_404

"""
Prevents users to view a conversation they are not part of.
"""


def is_creator_required(function):
    def wrap(request, *args, **kwargs):
        # import pdb; pdb.set_trace()
        user = request.user
        current_url = resolve(request.path_info).url_name
        pk = kwargs['pk']

        if (current_url == 'campaign_detail' or current_url == 'campaign_delete' or
            current_url == 'campaign_settings' or current_url == 'recipient_create' or
                current_url == 'recipient_upload') or current_url == 'recipient_update':
            instance = get_object_or_404(Campaign, id=pk)
        if current_url == 'email_template_update' or current_url == 'email_template_delete' or current_url == 'email_template_duplicate':
            instance = get_object_or_404(EmailTemplate, id=pk)
        if current_url == 'review_criteria_update' or current_url == 'review_criteria_delete':
            instance = get_object_or_404(CriteriaTemplate, id=pk)
        if current_url == 'review_page_template_update' or current_url == 'review_page_template_delete' or current_url == 'review_page_template_duplicate':
            instance = get_object_or_404(IntroTemplate, id=pk)
        if current_url == 'route_update' or current_url == 'route_delete':
            instance = get_object_or_404(Route, id=pk)
        if current_url == 'condition_update' or current_url == 'condition_delete':
            instance = get_object_or_404(Condition, id=pk)

        if instance.creator != user:
            raise PermissionDenied

        return function(request, *args, **kwargs)

    wrap.__doc__ = function.__doc__
    wrap.__name__ = function.__name__
    return wrap

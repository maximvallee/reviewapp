"""
DEFAULT PAGE TEMPLATE
"""
EVALUATION_HEADING_DEFAULT = "Leave a Review"
EVALUATION_SUBHEADING_DEFAULT = "Take a few seconds to review your experience with us."
COMMENTS_PLACEHOLDER_DEFAULT = "This is where you write your review. Explain what happened, and leave out offensive words. Keep your feedback honest, helpful, and constructive."
COMMENTS_HEADING_DEFAULT = "Care to share more?"
REDIRECT_HEADING_DEFAULT = "Care to Share More?"
REDIRECT_SUBHEADING_DEFAULT = "Would you mind describing your experience directly on one of our public review pages of your choice?"
# REDIRECT_SUBHEADING_DEFAULT = "Would you mind leaving your comment on one of our public review pages to help us build our reputation more transparently?"
THANKYOU_HEADING_DEFAULT = "Thank you!"
THANKYOU_SUBHEADING_DEFAULT = "Your comments will be reviewed to help improve our service."


"""
DEFAULT EMAIL TEMPLATE
"""
EMAIL_NAME_DEFAULT = "New template"
EMAIL_SUBJECT_DEFAULT = "Message from Isabella"
EMAIL_HEADING_DEFAULT = "New Message"
EMAIL_BODY_DEFAULT = "Hope you’re doing well. I just wanted to check in on you regarding your stay at {{address}}. How has your experience been so far? I know you must be really busy but I would really appreciate it if you could take just 2 mins to fill out this short survey."
EMAIL_SUB_HEADING_DEFAULT = "Why your response matters"
EMAIL_SUB_BODY_DEFAULT = "Whether it is to express positive comments or provide constructive criticism, your feedback allows us to improve the quality of our rental community. "
EMAIL_BUTTON_DEFAULT = "Submit Review"


"""
DEFAULT PALETTE
"""
# NAVBAR_COLOR_DEFAULT = '#2740a6'
# HEADING_COLOR_DEFAULT = '#121f3e'
# SUBHEADING_COLOR_DEFAULT = '#96a0b4'
# BODY_COLOR_DEFAULT = '#000000'
# BACKGROUND_COLOR_DEFAULT = '#f1f5f8'
# BUTTON_COLOR_DEFAULT = '#3554d1'

NAVBAR_COLOR_DEFAULT = '#03153a'
HEADING_COLOR_DEFAULT = '#121f3e'
SUBHEADING_COLOR_DEFAULT = '#89898B'
BODY_COLOR_DEFAULT = '#000000'
BACKGROUND_COLOR_DEFAULT = '#fafafa'
BUTTON_COLOR_DEFAULT = '#43bcad'


"""
DEFAULT CRITERIA TEMPLATE
"""
CRITERIA_LABEL_TEMPLATE = "How was your experience?"


"""
DEFAULT ORGANIZATION
"""
ORGANIZATION_NAME_DEFAULT = 'Your company'
ORGANIZATION_WEBSITE_DEFAULT = 'https://www.yourcompany.com'
# Not used in models. Front-end only.
ORGANIZATION_LOGO_DEFAULT = 'https://detour-app.s3.amazonaws.com/static/img/default_logo.jpg'
# Not used in models. Front-end only.
ORGANIZATION_ROUTE_DEFAULT = 'https://detour-app.s3.amazonaws.com/static/img/default_route.png'

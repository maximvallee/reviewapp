
from django.contrib import admin
from django.urls import path, include
from accounts import views

app_name = 'accounts'

urlpatterns = [
    path('profile/update', views.update_profile, name='profile_update'),
]
from django.shortcuts import render, redirect
from accounts.forms import *
from django.contrib.auth.decorators import login_required
from campaigns.utils import url_to_view


"""
Profile & Account Settings
"""


@login_required
def update_profile(request):

    instance = request.user

    if request.method == "POST":
        form = ProfileForm(request.POST, request.FILES)

        if form.is_valid():
            data = form.cleaned_data
            # import pdb; pdb.set_trace()
            instance.organization = data['organization']
            instance.website = data['website']
            if data['logo']:
                instance.logo = data['logo']
            elif data['logo'] == False:
                instance.logo = None
            if data['avatar']:
                instance.avatar = data['avatar']
            elif data['avatar'] == False:
                instance.avatar = None
            instance.first_name = data['first_name']
            instance.last_name = data['last_name']
            instance.phone = data['phone']
            instance.address = data['address']
            instance.city = data['city']
            instance.state = data['state']
            instance.zip_code = data['zip_code']
            instance.test_email = data['test_email']
            instance.save()
            return redirect('campaigns:index')
        else:
            context = {'form': form}
            return render(request, 'accounts/user_form.html', context)

    form = ProfileForm(instance=instance)

    context = {
        'form': form,
        'view_name': url_to_view(request.path)
    }

    return render(request, 'accounts/user_form.html', context)

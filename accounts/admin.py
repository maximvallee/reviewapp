from django.contrib import admin
from accounts.models import CustomUser

# Register your models here.
class UserAdmin(admin.ModelAdmin):
    list_display = ['id', 'email', 'username']

admin.site.register(CustomUser, UserAdmin)
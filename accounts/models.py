from django.db import models
from django.contrib.auth.models import AbstractUser
from django.db import transaction
from reviewproject.defaults import *
import re


class CustomUser(AbstractUser):
    organization = models.CharField(
        max_length=24, default=ORGANIZATION_NAME_DEFAULT)
    website = models.URLField(
        max_length=200, default=ORGANIZATION_WEBSITE_DEFAULT)
    logo = models.FileField(upload_to='media/logos', blank=True)
    avatar = models.FileField(upload_to='media/avatars', blank=True)
    phone = models.CharField(max_length=18, blank=True)
    address = models.CharField(max_length=48, blank=True)
    city = models.CharField(max_length=48, blank=True)
    state = models.CharField(max_length=18, blank=True)
    zip_code = models.CharField(max_length=10, blank=True)
    test_email = models.EmailField(max_length=64)

    def __str__(self):
        return self.username

    def clean_website_url(self):
        # import pdb; pdb.set_trace()
        url_clean = ''
        if self.website:
            url = re.compile(r"https?://(www\.)?")
            url_clean = "www." + url.sub('', self.website).strip().strip('/')
        return url_clean

    @transaction.atomic()
    # auto_create_templates_with_user() signal must be one single transaction
    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)

from django import forms
from accounts.models import *
from django.forms.widgets import TextInput


class ProfileForm(forms.ModelForm):

    # logo = forms.FileField(required=False)

    class Meta:
        model = CustomUser
        fields = [
            'organization',
            'website',
            'logo',
            'avatar',
            'first_name',
            'last_name',
            'phone',
            'address',
            'city',
            'state',
            'zip_code',
            'test_email'
        ]

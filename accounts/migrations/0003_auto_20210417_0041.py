# Generated by Django 3.2 on 2021-04-17 04:41

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0002_auto_20210408_0304'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='customuser',
            name='background_color',
        ),
        migrations.RemoveField(
            model_name='customuser',
            name='button_color',
        ),
    ]

# Generated by Django 3.2 on 2021-09-22 20:48

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0020_customuser_avatar'),
    ]

    operations = [
        migrations.AddField(
            model_name='customuser',
            name='test_email',
            field=models.EmailField(default='maximvallee@gmail.com', max_length=64),
            preserve_default=False,
        ),
    ]

# Generated by Django 3.2 on 2021-05-28 21:30

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0018_alter_customuser_logo'),
    ]

    operations = [
        migrations.AlterField(
            model_name='customuser',
            name='logo',
            field=models.FileField(blank=True, upload_to='media/logos'),
        ),
    ]
